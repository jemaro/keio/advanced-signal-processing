# Advanced Signal Processing

All the questions/tasks given below must be addressed properly in the report.
1. What is the “Hilbert space”? State explicitly how you understand it with
some complete example(s).
2. What is the “metric projection” and how is it different from the
“orthogonal projection”? Answer the questions using concrete example(s).
3. What is the “proximity operator” and why is it important in optimization?
The second question here could be answered from a theoretical viewpoint
or an application viewpoint, or both.
4. What is the “quasi-nonexpansive mapping” and how is it related to signal
processing? What is its subclass of mappings that is more relevant to
convergence guarantee? (Hint: This question is related to Mann
iteration.)
5. Apply some of the convex optimization methods that you have learned
through the course to your own problem, or any applications you like.
This part must include the following:
1) Short introduction: Background of the problem
2) Mathematical description of the method(s) you use
3) Simulation results / mathematical discussions
4) Discussion and conclusion
5) References
- Engineering students: Expected to show simulation results.