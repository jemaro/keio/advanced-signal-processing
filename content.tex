\section{Hilbert space}
% ## What is the “Hilbert space”? 
% > State explicitly how you understand it with some complete example(s).

A Hilbert space is a complete inner product space. Not to be confused with a
Banach space, which is a complete normed space. The existence of a norm can be
induced by the inner product while an inner product can not necessarily be
induced by the norm. Which means that all Hilbert spaces are Banach spaces, but
not all Banach spaces are Hilbert spaces.

An example of a Hilbert space is the Euclidean space, $\mathcal{H} =
\mathbb{R}^3$, a very common way to observe our underlying universe. With the
inner product defined in \cref{eq:inner-product} and the norm in
\cref{eq:norm}.

\begin{equation}
    \langle x, y\rangle  = \sum_{i=1}^3 x_i y_i
    \label{eq:inner-product}
\end{equation}

\begin{equation}
    \| x \|  = \sqrt{\sum_{i=1}^3 x_i^2}
    \label{eq:norm}
\end{equation}


\section{Orthogonal and Metric projections}

% What is the “metric projection” and how is it different from the “orthogonal
% projection”? 

Given a point $x$ in a Hilbert space $\mathcal{H}$ such that $x \in
\mathcal{H}$. The orthogonal projection is the best approximation of a point
$x$ in a closed \emph{subspace} of $\mathcal{H}$, $M$. While the metric
projection is the best approximation of a point $x$ in a closed \emph{convex
subset} of $\mathcal{H}$, $C$ such that $C \subset \mathcal{H}$. 

% Answer the questions using concrete example(s)

In the orthogonal projection $\langle x - P_M(x), m - P_M(x) \rangle = 0,
\forall m \in M$ while in the metric projection $\langle x - P_C(x), c - P_C(x)
\rangle \leq 0, \forall c \in C$. An example of a metric projection is the
projection of a point $x$ in the Euclidean space $\mathbb{R}^3$ to the unit
sphere subset located in the origin $C = {{x, y, z}, | x^2 + y^2 + z^2  \leq 1}
\subset \mathbb{R}^3$. Note that $C$ is not a \emph{subspace} of $\mathbb{R}^3$
and therefore we can't have its orthogonal projection. If we consider $M$ a
plane subspace of the Euclidean space $\mathbb{R}^3$ such that $M =
\mathbb{R}^2$ then we can have its orthogonal projection.


\section{Proximity Operator}

% > Answer the questions using concrete example(s). 8.1

% ## What is the “proximity operator” and why is it important in optimization? 

% > The second question here could be answered from a theoretical viewpoint or an
% > application viewpoint, or both.

Is a unique vector that minimizes a proper lower-semicontinuous convex function
$f$. The proximity operator of the indicator function of a closed convex set is
its projection. The proximity operator is a strict generalization of the
projection operator \cite{ProxOperator}.

In order to showcase the fact that the proximity operator is a generalization
of the projection operator, we will recall the proximity operator for a proper
lower-semicontinuous convex function $f$ as defined in
\cref{eq:proximity-operator}. The indicator function value is 0 if the given
point belongs to the indicated convex set, infinity otherwise. This makes it a
strong condition for minimization that allows us to deduce the projection
operator defined in \cref{eq:projection-operator} from the proximity operator.

\begin{equation}
    \mathrm{prox}_{\gamma f}(x) := 
    \argmin_{y} \left[ f(y) + \frac{1}{2 \gamma} \|x - y\|^2 \right]
    \label{eq:proximity-operator}
\end{equation}

\begin{equation}
    P_C(x) = \argmin_{y \in C} \|x-y\|^2
    \label{eq:projection-operator}
\end{equation}

The proximity operator is useful in the optimization of the composite problem,
which is of considerable importance in machine learning:

\begin{equation}
    \min_{x \in \mathcal{H}} \ell(x) + f(x)
\end{equation}

where $\ell(x)$ is usually some loss function and $f(x)$ represents a
regularizer, which is desirably nonsmooth, hance computationally harder to deal
with. The proximity operator can be used here with the Proximal Gradient
Algorithm and the Proximal Forward-backward Splitting Method.

\section{Quasi-Nonexpansive mapping}

% What is the “quasi-nonexpansive mapping” and how is it related to signal
% processing?

A quasi-nonexpansive mapping is a mapping $T : \mathcal{H} \to \mathcal{H}$
such that $\|T(x) - z\| \leq \|x - z\|, \forall x \in \mathcal{H}, \forall z
\in Fix(T)$, being $Fix(T)$ the fixed point set of the mapping $T$. This
implies that the mapping has a non empty fixed point set $Fix(T) \neq
\emptyset$ and that the distance between two points shrinks or remains equal
after being mapped. In this way it can be deduced that the fixed point set of a
quasi non-expansive mapping $T$, $Fix(T)$ is closed and convex. This properties
are very useful in signal processing as it allows us to use iterative methods
like the projected gradient method or projections onto convex sets.

% What is its subclass of mappings that is more relevant to convergence
% guarantee? 

% > (Hint: This question is related to Mann iteration.) 

Its subclass, averaged quasi-nonexpansive mappings play a regularizing role in
the convergence analysis of iterative algorithms \cite{QNM}.


\section{Convex optimization application}

\subsection{Problem: Parameter Estimation}
\label{subsec:problem}

The task of estimating the value of an unknown parameter vector has been at the
center of interest in a number of application areas. For example, in the early
years in the University, one of the very first tasks any student has to study
is the so-called curve fitting problem. Given a set of data points, one must
find a curve or a surface that \emph{fits} the data.

We will consider the regression model, relating input-output observation
points,

\begin{center}
    $y_n = h^T x_n + \eta_n, (y_n, x_n) \in \mathbb{R} \times \mathbb{R}^l, n \in
        \mathbb{N}$
\end{center}

where $h$ is the unknown parameter vector and $\eta_n$ is a bounded noise
sequence $|\eta_n| \leq \epsilon$.

\subsection{Method: Adaptive Projected Subgradient Method}
\label{subsec:method}

% Mathematical description
The adaptive projected subgradient method is an extension of the parallel
version of Projections Onto Convex Sets (POCS) algorithm. 

Let $\varphi_k : \mathcal{H} \rightarrow \left[0,\inf\right) \left(\forall k
    \in \mathbb{N}\right)$ be a sequence of continuous convex functions and $K
    \subset \mathcal{H}$ a nonempty closed convex set. For an arbitrarily given
$h_0 \in K$, the adaptive projected subgradient method produces a sequence
$(h_k)_{k \in \mathbb{N}} \subset K$ by \cref{eq:APSM}. Where
$\varphi^\prime_k(h_k) \in \partial\varphi_k(h_k)$ and $0 \leq \lambda_k \leq
    2$. \cite{YAMADA2004}.

\begin{equation}
    h_{k+1} := \begin{cases} 
        P_K\left(h_k - \lambda_k \frac{\varphi_k(h_k)}{\|\varphi^\prime_k(h_k)\|^2} \varphi^\prime_k(h_k)\right) & if \varphi^\prime_k(h_k) \neq 0, \\
        h_k                                                                                                      & otherwise,
    \end{cases}
    \label{eq:APSM}
\end{equation}

By setting the cost function $\varphi_k(x) := \sum_{\imath \in \mathcal{I}_k}
    w_\imath^{(k)} d^2_{C_\imath^{(k)}}(x), k \in \mathbb{N}$ where
$C_\imath^{(k)}$ is a closed convex set and $w_\imath^{(k)} > 0, \sum_{\imath
        \in \mathcal{I}_k} w_\imath^{(k)} = 1, k \in \mathbb{N}$ we can derive
\cref{eq:APSM_example} where $h_0 \in K, \mu_k \in \left[0,
    2\mathcal{M}_k^{(1)}\right]$ with the extrapolation coefficient defined in
\cref{eq:Mk}.

\begin{equation}
    h_{k+1} := 
    P_K\left[h_k + \mu_k \left(\sum_{\imath \in \mathcal{I}_k} w_\imath^{(k)} P_{C_\imath^{(k)}}(h_k) - h_k\right)\right]
    \label{eq:APSM_example}
\end{equation}

\begin{equation}
    M_k^{(1)} := \begin{cases} 
        \frac{
        \sum_{\imath \in \mathcal{I}_k} w_\imath^{(k)}
        \|P_{C_\imath^{(k)}}(h_k) - h_k\|^2}{
        \|\sum_{\imath \in \mathcal{I}_k} w_\imath^{(k)} P_{C_\imath^{(k)}}(h_k) - h_k\|^2 
        } & h_k \notin \bigcap_{\imath \in \mathcal{I}_k} C_\imath^{(k)} \\
        1 & otherwise,
    \end{cases}
    \label{eq:Mk}
\end{equation}

For our regression parameter estimation problem we define $\mathcal{I}_k$ to a
sliding window of size $q$. $C_\imath^{(k)}$ is a hyperslab $S_{\epsilon,
            \imath}$ constructed from an output-input pair of training data $\imath$ with
width $\epsilon$ \cite{ML}. 

\subsection{Simulation}
\label{subsec:simulation}

The code used to simulate the method described in \cref{subsec:method} is
available in the
\href{https://gitlab.com/jemaro/keio/advanced-signal-processing/-/blob/main/Advanced%20Signal%20Processing.ipynb}{code
    repository}. 

First we check the behavior of the method with regression synthetic data. We
generate 3500 random input readings, set a random reference system $h^*$ and
compute the system output with white noise of zero mean and $0.1$ standard
deviation (spread or \emph{width} of the noise).

\Cref{fig:simulation-random-epsilon} illustrates how the method behaves with
different values of the hyperslab width $\epsilon$. The bigger the width of the
hyperslab, the steadier its value becomes with more iterations. But the closer
the width is to the noise standard deviation, the better the converged value.

\Cref{fig:simulation-random-q} shows how the method behaves with the size of
the sliding window $q$. The bigger the window, the more data is reused between
iterations, and therefore the faster the convergence. It has a relatively small
effect on the converged value, but it has a significant impact on the
computational cost. Although this effect can be palliated by parallelizing the
projections withing each iteration.

\begin{figure}
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{simulation-random-epsilon.png}
        \caption{Mean Squared Error per iteration using synthetic data with
            different values of $\epsilon$.}
        \label{fig:simulation-random-epsilon}
    \end{subfigure}
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{simulation-random-q.png}
        \caption{Mean Squared Error per iteration using synthetic data with
            different values of $q$.}
        \label{fig:simulation-random-q}
    \end{subfigure}
\end{figure}

Now we can apply this method to real data, like the one available in the
challenge
\href{https://www.kaggle.com/c/house-prices-advanced-regression-techniques}{House
Prices - Advanced Regression Techniques}. The dataset contains different house
features and their sale price. The goal is to estimate the sale price of a
house knowing some of the features. As the ground truth system is unknown, we
will divide our dataset in \emph{train} and \emph{test} with a random split of
80\% towards training. We will calculate the mean square error after each
iteration of the method between the known house price and the estimated house
price with both splits (\emph{train} and \emph{test}). But only the
\emph{train} split is used to calculate new system estimations.
\Cref{fig:simulation-kaggle} shows the results of our simulation. Details about
which of the available features where used can be found in the
\href{https://gitlab.com/jemaro/keio/advanced-signal-processing/-/blob/main/Advanced%20Signal%20Processing.ipynb}{code
    repository}. For this simulation we set $\epsilon = 1, q = 5, \mu_k = 0.5
    \mathcal{M}_k^{(1)}$. One can see that the error between the estimated and real
house price decreases significantly with the number of iterations in both data
splits, even though \emph{test} data was not used to improve the estimation.

\begin{figure}
    \centering
    \includegraphics{simulation-kaggle.png}
    \caption{Mean Squared Error per iteration for the House Prices dataset.}
    \label{fig:simulation-kaggle}
\end{figure}

\subsection{Conclusion}

Parameter estimation is an important problem nowadays with the large amounts of
data available. We have successfully implemented a convex optimization method
and used it to solve the regression parameter estimation problem. First with
synthetic data, where we have explored the influence of the different
parameters that we can tune. Then with real data, where our implementation
succeeded to converge to a solution.
